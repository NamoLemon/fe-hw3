/*Домашня робота
1. Создайте массив styles с элементами «Джаз» и «Блюз».
2. Добавьте «Рок-н-ролл» в конец.
3. Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
4. Удалите первый элемент массива и покажите его.
5. Вставьте «Рэп» и «Регги» в начало массива.
Повторить все то что проходили на уроке */

//1. Создайте массив styles с элементами «Джаз» и «Блюз».
var style = ["Джаз", " Блюз"] ;

document.write (style);
document.write ('<hr/>');

//2. Добавьте «Рок-н-ролл» в конец.
style [3] = " Рок-н-ролл";
document.write (style);
document.write ('<hr/>');

//3. Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
var midleNum = (styles.length%2 != 1) ? styles.length/2 : Math.round(styles.length/2) - 1;
style[midleNum] = 'Классика';
document.write (style);
document.write ('<hr/>');

//4. Удалите первый элемент массива и покажите его.
var value = style.shift();
document.write (value);
document.write (style);
document.write ('<hr/>');

//5. Вставьте «Рэп» и «Регги» в начало массива.
var value2 = style.unshift('Рэп', 'Регги');
document.write (value2);
document.write (style);
document.write ('<hr/>');

